package com.service.sdztest1.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-08-06T02:03:03.533Z")

@RestSchema(schemaId = "sdztest1")
@RequestMapping(path = "/sdztest1", produces = MediaType.APPLICATION_JSON)
public class Sdztest1Impl {

    @Autowired
    private Sdztest1Delegate userSdztest1Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userSdztest1Delegate.helloworld(name);
    }

}
