package com.service.sdztest1.controller;

import org.springframework.stereotype.Component;


@Component
public class Sdztest1Delegate {

    public String helloworld(String name){

        // Do Some Magic Here!
        return name;
    }
}
