package com.service.sdztest1.controller;



import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestSdztest1 {

        Sdztest1Delegate sdztest1Delegate = new Sdztest1Delegate();


    @Test
    public void testhelloworld(){

        String expactReturnValue = "hello"; // You should put the expect String type value here.

        String returnValue = sdztest1Delegate.helloworld("hello");

        assertEquals(expactReturnValue, returnValue);
    }

}